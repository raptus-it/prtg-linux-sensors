#!/bin/bash

###
# Required configurations for Nginx / PHP-FPM for this sensor
#
# Nginx inside corresponding server block:
#
#    location /nginx-status {
#        stub_status             on;
#        allow 127.0.0.1;
#        deny                    all;
#    }
#
# PHP-FPM configuration:
#
#    pm.status_path = /php-fpm-status
#    ping.path = /php-fpm-ping
#    ping.response = pong
#

URL_NGINX="$1/nginx-status"
URL_PHPFPM="$1/php-fpm-status"
STATE_NGINX="/tmp/nginx-state-check-`uuid`.state"
STATE_PHPFPM="/tmp/phpfpm-state-check-`uuid`.state"

function returnError {
    if test -f "$STATE_NGINX"
    then
        rm -f "$STATE_NGINX"
    fi

    if test -f "$STATE_PHPFPM"
    then
        rm -f "$STATE_PHPFPM"
    fi

    eid="$1"
    err="$2"
    echo "<prtg>"
    echo "<error>$eid</error>"
    echo "<text>$err</text>"
    echo "</prtg>"
    exit $err
}

rc="`curl -sw '%{http_code}' -o $STATE_NGINX $URL_NGINX`"
if ! test "$rc" = "200"
then
    returnError "FAILED to obtain Nginx state HTTP Code: $rc" 2
fi

rc="`curl -sw '%{http_code}' -o $STATE_PHPFPM $URL_PHPFPM`"
if ! test "$rc" = "200"
then
    returnError "FAILED to obtain PHP-FPM state HTTP Code: $rc" 2
fi

nginx_actconn="`cat $STATE_NGINX | sed -rn 's/^Active connections: ([0-9]*)/\1/p'`"
phpfpm_listq="`cat $STATE_PHPFPM | sed -rn 's/^listen queue: ([0-9]*)/\1/p'`"
phpfpm_listql="`cat $STATE_PHPFPM | sed -rn 's/^listen queue len: ([0-9]*)/\1/p'`"
phpfpm_idlep="`cat $STATE_PHPFPM | sed -rn 's/^idle processes: ([0-9]*)/\1/p'`"
phpfpm_actp="`cat $STATE_PHPFPM | sed -rn 's/^active processes: ([0-9]*)/\1/p'`"
phpfpm_totp="`cat $STATE_PHPFPM | sed -rn 's/^total processes: ([0-9]*)/\1/p'`"
phpfpm_maxch="`cat $STATE_PHPFPM | sed -rn 's/^max children reached: ([0-9]*)/\1/p'`"

echo "<prtg>"
echo "<result><channel>Nginx active connections</channel><value>$(echo $nginx_actconn)</value><unit>Count</unit><mode>Absolute</mode></result>"

echo "<result><channel>PHP-FPM listen queue</channel><value>$(echo $phpfpm_listq)</value><unit>Count</unit><mode>Absolute</mode></result>"

echo "<result><channel>PHP-FPM listen queue length</channel><value>$(echo $phpfpm_listql)</value><unit>Count</unit><mode>Absolute</mode></result>"

echo "<result><channel>PHP-FPM idle processes</channel><value>$(echo $phpfpm_idlep)</value><unit>Count</unit><mode>Absolute</mode></result>"

echo "<result><channel>PHP-FPM active processes</channel><value>$(echo $phpfpm_actp)</value><unit>Count</unit><mode>Absolute</mode></result>"

echo "<result><channel>PHP-FPM total processes</channel><value>$(echo $phpfpm_totp)</value><unit>Count</unit><mode>Absolute</mode></result>"

echo "<result><channel>PHP-FPM max children reached</channel><value>$(echo $phpfpm_maxch)</value><unit>Count</unit><mode>Absolute</mode></result>"

echo "</prtg>"

# eof
