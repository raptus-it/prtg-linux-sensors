#!/bin/bash
#
# this script must return an xml according to:
#   https://monitoring.raptus.com/help/custom_sensors.htm
if [ $( id -u ) -ne 0 ]; then
  sudo /var/prtg/scriptsxml/wp-import-state.sh "$1"
  exit;
fi

if test -z "$1"
then
    echo "<prtg><error>1</error><text>First parameter must be: domain.tld</text></prtg>"
    exit
fi

BASE="/home/sites/$1"
TRANSIT="$BASE/transit/mdm-to-shp"

if [ ! -d "$TRANSIT" ]
then
    echo "<prtg><error>1</error><text>Transit directory does not exist: $TRANSIT</text></prtg>"
    exit
fi

cd $TRANSIT

echo "<prtg>"

for d in */ ; do
    channel=${d/chan-/}
    channel=${channel/\//}
    state=0
    if [ ! -f "$d.lock" ] && [ -f "$d.date" ] ; then
        state=2
    elif [ -f "$d.local.lock" ] ; then
        state=1
    fi
        echo "<result><channel>$channel</channel><value>$state</value><unit>Count</unit><mode>Absolute</mode><LimitMode>1</LimitMode><LimitMaxError>1</LimitMaxError></result>"
done

echo "</prtg>"

# eof
