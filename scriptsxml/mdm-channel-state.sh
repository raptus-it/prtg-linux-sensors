#!/bin/bash
#
# this script must return an xml according to:
#   https://monitoring.raptus.com/help/custom_sensors.htm

###
# MDM should support channel state monitoring out of the box

if test -z "$1"
then
    echo "<prtg><error>1</error><text>First parameter must be: https://pim.domain.tld/</text></prtg>"
    exit
fi

URL="$1/channel.state"
STATEFILE="/tmp/mdm-channel-state-check-`uuid`.state"

function removeStateFile 
{
    if test -f "$STATEFILE"
    then
        rm -f "$STATEFILE"
    fi
}

rc="`curl -k -sw '%{http_code}' -o $STATEFILE $URL`"
if ! test "$rc" = "200"
then
    removeStateFile
    echo "<prtg><error>1</error><text>FAILED to obtain MDM channel state. HTTP Code: $rc</text></prtg>"
    exit
fi

echo "<prtg>"

while IFS= read -r line
do
    state=`echo $line | cut -d':' -f1`
    channel=`echo $line | cut -d':' -f2`
    echo "<result><channel>$channel</channel><value>$state</value><unit>Count</unit><mode>Absolute</mode><LimitMode>1</LimitMode><LimitMaxWarning>0</LimitMaxWarning><LimitMaxError>1</LimitMaxError></result>"
done < "$STATEFILE"

removeStateFile

echo "</prtg>"

# eof
