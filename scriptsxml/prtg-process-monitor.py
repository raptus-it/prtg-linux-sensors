#!/usr/bin/python
# Monitoring of one process in linux
# If multiple processes are found, they are summarized

import os
import sys

# if not root, call myself with sudo
uid = os.getuid()
if uid != 0:
    args = sys.argv[1:]
    sudo_cmd = ['sudo', '/var/prtg/scriptsxml/prtg-process-monitor.py']+args
    os.system(' '.join(sudo_cmd))
    sys.exit()


def returnError(eid, err):
    print("<prtg>")
    print("<error>" + str(eid) + "</error>")
    print("<text>" + err + "</text>")
    print("</prtg>")
    sys.exit()


try:
    import psutil
except ImportError:
    returnError(1, "Python package PSUTIL missing. Please install it.")

if not psutil.__version__[0:1] >= "2":
    returnError(1, "Python package PSUTIL needs update > v2.x.")

if len(sys.argv) < 2:
    returnError(2, "Process name missing")

# Name of the dameon or the process (for example: mysqld)
PROCNAME = sys.argv[1]

# If defined: Filter the list of found processes
PROCIDSTRING = ""
if len(sys.argv) >= 3:
    PROCIDSTRING=sys.argv[2]

# Compile data
PROCDATA = []
try:
    for proc in psutil.process_iter():
        if not proc.name() == PROCNAME:
                continue
        if PROCIDSTRING:
            if not any(PROCIDSTRING in item for item in proc.cmdline()):
                continue

        pcpu = proc.cpu_percent(interval=0.3)
        mem = proc.memory_info()
        files = len(proc.open_files())
        childs = len(proc.children())
        conns = len(proc.connections())
        io = proc.io_counters()
        PROCDATA.append( [pcpu, mem[0], mem[1], files, childs, conns, io[2], io[3]] )

    PROC_COUNT = 0
    PROC_CPU = 0
    PROC_RMEM = 0
    PROC_VMEM = 0
    PROC_FILES = 0
    PROC_CHILDS = 0
    PROC_CONNS = 0
    PROC_IOREAD = 0
    PROC_IOWRITE = 0
    if not PROCDATA:
        returnError(2, "No processes found.")
    else:
        PROC_COUNT = len(PROCDATA)
        for proc in PROCDATA:
            PROC_CPU = PROC_CPU + float(proc[0])
            PROC_RMEM = PROC_RMEM + float(proc[1])
            PROC_VMEM = PROC_VMEM + float(proc[2])
            PROC_FILES = PROC_FILES + float(proc[3])
            PROC_CHILDS = PROC_CHILDS + float(proc[4])
            PROC_CONNS = PROC_CONNS + float(proc[5])
            PROC_IOREAD = PROC_CONNS + float(proc[6])
            PROC_IOWRITE = PROC_CONNS + float(proc[7])

        PROC_CPU = PROC_CPU / PROC_COUNT
        PROC_RMEM = int(PROC_RMEM / PROC_COUNT)
        PROC_VMEM = int(PROC_VMEM / PROC_COUNT)
        PROC_FILES = int(PROC_FILES / PROC_COUNT)
        PROC_CHILDS = int(PROC_CHILDS / PROC_COUNT)
        PROC_CONNS = int(PROC_CONNS / PROC_COUNT)
        PROC_IOREAD = int(PROC_IOREAD / PROC_COUNT)
        PROC_IOWRITE = int(PROC_IOWRITE / PROC_COUNT)

    print("<prtg>")
    print("<result><channel>Process count</channel><value>"+str(PROC_COUNT)+"</value><unit>Count</unit><mode>Absolute</mode></result>")

    print("<result><channel>CPU %</channel><value>"+format(PROC_CPU, '.2f')+"</value><float>1</float><unit>Percent</unit><mode>Absolute</mode></result>")

    print("<result><channel>Resident Memory</channel><value>"+str(PROC_RMEM)+"</value><float>0</float><unit>BytesMemory</unit><mode>Absolute</mode></result>")

    print("<result><channel>Virtual Memory</channel><value>"+str(PROC_VMEM)+"</value><float>0</float><unit>BytesMemory</unit><mode>Absolute</mode></result>")

    print("<result><channel>Open Files</channel><value>"+str(PROC_FILES)+"</value><unit>Count</unit><mode>Absolute</mode></result>")

    print("<result><channel>Child processes</channel><value>"+str(PROC_CHILDS)+"</value><unit>Count</unit><mode>Absolute</mode></result>")

    print("<result><channel>Connections</channel><value>"+str(PROC_CONNS)+"</value><unit>Count</unit><mode>Absolute</mode></result>")

    print("<result><channel>I/O Read</channel><value>"+str(PROC_IOREAD)+"</value><unit>BytesDisk</unit><mode>Absolute</mode></result>")

    print("<result><channel>I/O Write</channel><value>"+str(PROC_IOWRITE)+"</value><unit>BytesDisk</unit><mode>Absolute</mode></result>")
    print("</prtg>")

except Exception as e:
    returnError(1, "EXCEPTION: " + str(e))

# eof
