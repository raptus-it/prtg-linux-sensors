#!/bin/bash
#
# this script must return:  returncode:value:message
#
# Value has to be a 64 bit integer or float and will be used as the 
# resulting value for this sensor (e.g. bytes, milliseconds, etc.), message 
# can be any string (maximum length: 2000 characters) and will be stored 
# in the database.
#
# The SSH script's "returncode" has to be one of the following values:
#
# Value Description
# 0     OK
# 1     WARNING
# 2     System Error (e.g. a network/socket error)
# 3     Protocol Error (e.g. web server returns a 404)
# 4     Content Error (e.g. a web page does not contain a required word)

###
# Required configurations for Nginx / PHP-FPM for this sensor
#
# Nginx inside corresponding server block:
#
#    location ~ ^/(php-fpm-status|php-fpm-ping)$ {
#        include                 fastcgi_params;
#        fastcgi_pass            unix:/home/sites/www.domain.tld/run/php-fpm.sock;
#        fastcgi_temp_path       /home/sites/www.domain.tld/pub/tmp/fcgi 1 2;
#        fastcgi_param           SCRIPT_FILENAME $document_root$fastcgi_script_name;
#        fastcgi_param           HTTPS on;
#        allow 127.0.0.1;
#        deny                    all;
#    }
#
# PHP-FPM configuration:
#
#    pm.status_path = /php-fpm-status
#    ping.path = /php-fpm-ping
#    ping.response = pong
#

if test -z "$1"
then
    echo "1:1:First parameter must be: http://www.domain.tld/"
    exit
fi

URL="$1/php-fpm-ping"
STATEFILE="/tmp/phpfpm-ping-check-`uuid`.state"

function removeStateFile 
{
    if test -f "$STATEFILE"
    then
        rm -f "$STATEFILE"
    fi
}

rc="`curl -sw '%{http_code}' -o $STATEFILE $URL`"
if ! test "$rc" = "200"
then
    removeStateFile
    echo "2:0:FAILED to obtain PHP-FPM ping HTTP Code: $rc"
    exit 2
fi

if test -z "`cat $STATEFILE | grep 'pong'`"
then
    removeStateFile
    echo "2:0:FAILED to get PONG response from PHP-FPM!"
    exit 2
fi

removeStateFile
echo "0:0:OK"
exit 0

# eof
