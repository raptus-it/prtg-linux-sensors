#!/bin/bash
#
# this script must return:  returncode:value:message
#
# Value has to be a 64 bit integer or float and will be used as the 
# resulting value for this sensor (e.g. bytes, milliseconds, etc.), message 
# can be any string (maximum length: 2000 characters) and will be stored 
# in the database.
#
# The SSH script's "returncode" has to be one of the following values:
#
# Value Description
# 0     OK
# 1     WARNING
# 2     System Error (e.g. a network/socket error)
# 3     Protocol Error (e.g. web server returns a 404)
# 4     Content Error (e.g. a web page does not contain a required word)

if test -z "$1"
then
    echo "1:1:First parameter must be the local mount point. Example: /mnt/sshfs/remote.domain.tld/folder"
    exit
fi

LOCALMOUNT="$1"

# verify current mounts
if test -z "`cat /proc/mounts | grep $LOCALMOUNT`"
then
    echo "2:0:Mountpoint $LOCALMOUNT not found in /proc/mounts"
    exit 2
fi

# verify mount is sshfs
if test -z "`mount | grep "$LOCALMOUNT type fuse.sshfs"`"
then
    echo "2:0:Mountpoint $LOCALMOUNT is not a fuse block device"
    exit 2
fi

# verify sshfs process is here and unique
SSHFSPROCCOUNT="`pgrep -f ^sshfs.*$LOCALMOUNT | wc -l`"
if ! test "$SSHFSPROCCOUNT" = "1"
then
    echo "2:0:Number of SSHFS processes for $LOCALMOUNT is not 1, but $SSHFSPROCCOUNT"
    exit 2
fi

echo "0:0:OK"
exit 0

# eof
