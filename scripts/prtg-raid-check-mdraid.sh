#!/bin/bash
#
# this script must return:  returncode:value:message
#
# Value has to be a 64 bit integer or float and will be used as the 
# resulting value for this sensor (e.g. bytes, milliseconds, etc.), message 
# can be any string (maximum length: 2000 characters) and will be stored 
# in the database.
#
# The SSH script's "returncode" has to be one of the following values:
#
# Value Description
# 0     OK
# 1     WARNING
# 2     System Error (e.g. a network/socket error)
# 3     Protocol Error (e.g. web server returns a 404)
# 4     Content Error (e.g. a web page does not contain a required word)

if test -z "$1"
then
    echo "1:1:Specify parameter mdarr=n to this script"
    exit
fi

MDARR="`echo $1 | cut -d= -f2`"

if ! test -e /dev/md$MDARR
then
    echo "1:0:Cannot find /dev/md$MDARR"
    exit
fi

mdadm --misc --test /dev/md$MDARR
if ! [ $? = 0 ]
then 
    echo "2:0:Problem with Software RAID"
    exit
fi 

echo "0:0:OK"

# eof
