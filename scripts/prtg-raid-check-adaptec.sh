#!/bin/bash
#
# this script must return:  returncode:value:message
#
# Value has to be a 64 bit integer or float and will be used as the 
# resulting value for this sensor (e.g. bytes, milliseconds, etc.), message 
# can be any string (maximum length: 2000 characters) and will be stored 
# in the database.
#
# The SSH script's "returncode" has to be one of the following values:
#
# Value Description
# 0     OK
# 1     WARNING
# 2     System Error (e.g. a network/socket error)
# 3     Protocol Error (e.g. web server returns a 404)
# 4     Content Error (e.g. a web page does not contain a required word)

if test -z "$1"
then
    echo "1:1:Specify parameter ctrl=n to this script"
    exit
fi

CTRLR="`echo $1 | cut -d= -f2`"

STAT="`arcconf GETCONFIG $CTRLR AD`"
if test -z "`echo $STAT | egrep 'Logical devices\/Failed\/Degraded.*: .\/0\/0'`"
then
    echo "2:0:Problem with one of the RAID logical drives"
    exit
fi 

if test -z "`echo $STAT | egrep 'Controller Status.*\: Optimal'`"
then
    echo "2:0:Problem with RAID Controller"
    exit 
fi

echo "0:0:OK"

# eof
